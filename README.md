# lai2wrf 


This Repository gives information on how to update leaf area index (LAI) in WRF simulations. LAI information by default in WRF are based on MODIS satellite data (tables or maps), that are for some areas and land use categories too low (check [Warrach-Sagi et al. 2022](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2022JD036518) ) 

Here you can find guidelines how to use LAI data based on SPOT satellite downloaded from [CDS](https://cds.climate.copernicus.eu/cdsapp#!/dataset/satellite-lai-fapar?tab=form).

The Repository is organized in 2 folders:

1. In the [lai_preprocessing](./lai_preprocessing) folder all the steps and scripts how to obtain and calculate the monthly LAI from files downloaded from CDS, remap the data to the WRF grid and ingest the data manually. Furthermore, the folder contains the information how to update the table data for LAI for the MPTABLE.TBL (when running WRF with NOAH-MP model), to get more realistic values (e.g. higher values for croplands). The Updated table is provided ad well 

2. In the [lai4geogrid](./lai4geogrid) folder are given guidelines how to use global LAI data based on SPOT satellite information prepared to be readable directly by geogrid.exe that can be downloaded from [zenodo](to be uploaded)
