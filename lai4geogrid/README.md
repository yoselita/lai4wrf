# lai2wps 

Guidelines how to use LAI global data created based on information from 1999-2013 SPOT satellite information to obtain static maps readable bay WRF.
=============

Global data for leaf area index (LAI) based on SPOT satellite information has been converted and prepared to be readable by WPS, when running geogrid.exe. To be able to use the data it is necessary to perform these steps:

1. Download the data from [zenodo](link to be provided), unzip the folder and place the folder where all geographical static information are located - typically the folder name is WPS_GEOG

2. In the geogrid table GEOGRID.TBL add these lines above the line starting with "name=LAI12M" :
```
===============================
name=LAI12M
        priority=2
        dest_type=continuous
        interp_option=default:average_gcell(4.0)+four_pt+average_4pt+average_16pt+search
        z_dim_name=month
        masked = water
        fill_missing = 255.
        rel_path=default:lai_spot4_1km/
        flag_in_output=FLAG_LAI12M'
```
Example you can fine in [data directory](./data/GEOGRID.TBL)

3. In the namelist.wps the new data set will be used when including this line: 
```
geog_data_res  = 'default',
```

4. Note that for WPS domains that cover far north or far south, there might appear missing values for some months due to the position of the SPOT satellite that was not covering the areas. In that case it is necessary to run geogrid.exe with the default MODIS data (comment out the added lines indicated in pont 2), run geogrid.exe again, and fill the gaps with the data from MODIS. For more details, check the [notebook](./spotLAI2wps.ipynb) how this can be done, and for an example there are 2 geogrid files covering EUR11 CORDEX domain with both MODIS and SPOT LAI in the [data directory](./data)
